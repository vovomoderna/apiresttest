# Exemplo simples de app ApiRest utilizando Spring+Vue.js.  
## Tambem consome uma open api que devolve preço atual do bitcoin em varias moedas:
```bash
https://api.coindesk.com/v1/bpi/currentprice.json
```

## O que precisamos para buildar o projeto:
### Maven, NPM, Java8+, a aplicação esta configurada para rodar na porta 9595.

## Baixando (clonando) o projeto
**1. Git Clone. Favor desconsider o nome do projeto "vovomoderda", um (antigo projeto do Bitbucket) :).**

```
git clone https://thiagolg123@bitbucket.org/vovomoderna/apiresttest.git
```

## Construindo a parte Vue.js
### Entrar na pasta ..\src\main\app
```
npm install
```

### hot-reloads para desenvolvimento (somente para tempo de dev.)
```
npm run serve
```

### Compilar para "produção"
```
npm run build
```

## Construindo a parte Java

**1. Build com maven**

```bash
mvn package
java -jar target/thiago-goncalves-rest-api-0.0.1-SNAPSHOT.jar
```

Pronto! --> <http://localhost:9595>.

##### Considerações: ainda há alguns bugs na tabela de CRUD de usuarios, mas é possivel testar a API, fico devendo tambem a doc. utilizando o swagger. Não consegui muito tempo para deixar tudo redondo, Obrigado! :)