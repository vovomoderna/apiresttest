package br.com.thiago.simpleApi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.thiago.simpleApi.model.User;

/**
 * @author Thiago Goncalves
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {}
