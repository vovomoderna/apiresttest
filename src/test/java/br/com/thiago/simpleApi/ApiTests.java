package br.com.thiago.simpleApi;

import br.com.thiago.simpleApi.Application;
import br.com.thiago.simpleApi.model.User;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String appUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void testGetUsers() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = 
				restTemplate.exchange(appUrl() + "/users",
					HttpMethod.GET,
					entity,
					String.class);

		Assert.assertNotNull(response.getBody());
	}

	@Test
	public void testUserById() {
		User user = restTemplate.getForObject(appUrl() + "/users/1", User.class);
		
		Assert.assertNotNull(user);
	}

	@Test
	public void testPostCreateUser() {
		User user = new User();
		user.setNome("Thiago");
		user.setEmail("t@t.com");

		ResponseEntity<User> postResponse = restTemplate.postForEntity(appUrl() + "/users", user, User.class);
		Assert.assertNotNull(postResponse);
		Assert.assertNotNull(postResponse.getBody());
	}

	@Test
	public void testUpdatePost() {
		int id = 1;
		User user = restTemplate.getForObject(appUrl() + "/users/" + id, User.class);
		user.setNome("Thiago");
		user.setEmail("t@t.com");
		restTemplate.put(appUrl() + "/users/" + id, user);

		User updatedUser = restTemplate.getForObject(appUrl() + "/users/" + id, User.class);
		Assert.assertNotNull(updatedUser);
	}

	@Test
	public void testDeleteUser() {
		int id = 2;
		User user = restTemplate.getForObject(appUrl() + "/users/" + id, User.class);
		Assert.assertNotNull(user);

		restTemplate.delete(appUrl() + "/users/" + id);

		try {
			user = restTemplate.getForObject(appUrl() + "/users/" + id, User.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}

}
