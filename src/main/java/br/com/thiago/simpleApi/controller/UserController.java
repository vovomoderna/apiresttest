package br.com.thiago.simpleApi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.thiago.simpleApi.exception.HttpNotFoundException;
import br.com.thiago.simpleApi.model.User;
import br.com.thiago.simpleApi.repository.UserRepository;

/**
 *
 * @author Thiago Goncalves
 */

@RestController
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@GetMapping("/users")
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@GetMapping("/users/{id}")
	public ResponseEntity<User> findUsersById(@PathVariable(value = "id") Long userId) throws HttpNotFoundException {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new HttpNotFoundException("Problemas para encontrar Usuario: " + userId));
		return ResponseEntity.ok().body(user);
	}

	@PostMapping("/users")
	public User saveUser(@Valid @RequestBody User user) {
		return userRepository.save(user);
	}

	@PutMapping("/users/{id}")
	public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long userId,
			@Valid @RequestBody User userDetails) throws HttpNotFoundException {

		User user = userRepository
				.findById(userId)
				.orElseThrow(() -> new HttpNotFoundException("Problemas para encontrar Usuario: " + userId));

		user.setNome(userDetails.getNome());
		user.setEmail(userDetails.getEmail());
		final User userUpdate = userRepository.save(user);
		return ResponseEntity.ok(userUpdate);
	}

	@DeleteMapping("/users/{id}")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId) throws Exception {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new HttpNotFoundException("Problemas para deletar Usuario: " + userId));

		userRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
