package br.com.thiago.simpleApi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 
 * @author Thiago Goncalves
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class HttpNotFoundException extends Exception {

	private static final long serialVersionUID = -3642099024371621364L;

	public HttpNotFoundException(String message) {
		super(message);
	}
}
